import Character from "./characters/Character";

export default class Battle {
    fight(a: Character, b: Character) {
        if (!a || !b) {
            console.log('il manque un ou plusieurs combattants');
            // on quitte la methode fight avec return
            return;
        }
        while (a.get_health() > 0 && b.get_health() > 0) {
            console.log(`${b.name} attaque et frappe à: ${b.attack(b.etat)}`);
            a.set_health(b.attack(a.etat), a.race);

            //console.log(`${b.name} attaque et frappe à: ${b.attack()}`);
            a.set_health(b.attack(a.etat), a.race);
            console.log(`Il reste à ${a.name} : ${a.get_health()} pv`);
            if (a.get_health() > 0) {

                console.log(`${a.name} attaque et frappe à: ${a.attack(a.etat)}`);
                b.set_health(a.attack(a.etat), a.race);
                console.log(`Il reste à ${b.name} : ${b.get_health()} pv`);
            }
        }
        if (a.get_health() > 0 && b.get_health() <= 0) {
            a.set_xp();
            a.die();
            return console.log(`${a.name} a gagné et il lui reste: ${a.get_health()} pv et il a gagné ${a.get_xp()} xp`)
        }
        else {
            return console.log(`${a.name} a perdu face à ${b.name}, c'est pitoyable... Et en plus il lui reste: ${b.get_health()} pv`);
        }
    }
}
//Système de level 