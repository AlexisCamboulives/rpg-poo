import Character from "./Character";
import { TConfig } from "../types";
export default class Griffin extends Character {
   health: number;
   etat: "fly" | "sol" | "attackFromSky" = "sol";
   super(config: TConfig) { }
   

   set_health(damage: number) {
      if (this.etat == "fly") {
         this.health -= damage * 0.90;
      }
         this.health -= damage;
      
   }

   /*Le Dragon a une resistence supllementaire de 50%*/


   attack(): number {
      if (this.etat == "sol") {
         this.etat = "fly";
         console.log("Je suis au sol, pauvre de moi...")
         return this.hitStrength;
      }
      else if (this.etat == "fly") {
         this.etat = "attackFromSky";
         console.log("Le Griffin est en vol et n'attaque pas")
         return 0
      } else {
         console.log("Le Griffin attaque depuis les cieux et frappe 10% plus fort")
         this.etat = "sol";
         return this.hitStrength * 1.10;
      }
   }

}