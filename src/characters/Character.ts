// Les proprietes et methodes communes a tous les personnages
import { TConfig } from "../types";

export default class Character {

    name: string;
    health: number;
    hitStrength: number;
    lvl: number;
    xp: number;
    race: string;
    etat: string;


    constructor(config: TConfig)  {
        this.health = config.health;
        this.hitStrength = config.hitStrength;
        this.name = config.name;
        this.xp = config.xp;
        this.lvl = config.lvl;
        this.race = config.race;
    }

    /*Methodes communes à tout le monde**/
    get_name(){
        return this.name;
    }
    set_name(){
        this.name;
    }
    die() {
        return this.health += 10;
    }
    get_level(){
        return this.lvl;
    }
    set_level(){
        this.lvl;
    }
    get_xp(){
        return this.xp;
    }
    set_xp(){
        this.xp;
    }
    get_hitStrength(){
        return this.hitStrength
    }
    set_hitStrength(){
        this.hitStrength;
    }
    get_health(){
        return this.health; 
    }

    set_health(damage: number, etat: string){
        this.health -= damage;
        
    }
    attack(etat: string){
        return this.hitStrength * this.lvl;
    }
    get_race(){
        return this.race;
    }
    set_race(etat: string){
        this.race;
    }
}