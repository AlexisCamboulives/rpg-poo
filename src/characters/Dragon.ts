import Character from "./Character";
import { TConfig } from "../types";
export default class Dragon extends Character {
    health: number;
    etat: "fly"|"sol"|"attackFromSky" = "sol";
    level: number;
    super(config: TConfig) { }
    
    
    set_health(damage: number) {
        if (this.etat == "fly") {
            this.health -= damage * 0.40;
        } else {
            this.health -= damage * 0.50;
        }
    }

    /*Le Dragon a une resistence supllementaire de 50%*/


    attack(): number {
        if (this.etat == "sol") {
            this.etat = "fly";
            return this.hitStrength;
        }
        else if (this.etat == "fly") {
            this.etat = "attackFromSky";
            console.log("Le dragon est en vol et n'attaque pas")
            return 0
        }else {
            this.etat = "sol";
            console.log("Le dragon attaque depuis les cieux et frappe 10% plus fort")
            return this.hitStrength * 1.10;
        }
    }
}

