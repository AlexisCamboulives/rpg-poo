import Character from "./Character";
import { TConfig } from "../types";
export default class Assasin extends Character {
    super(config: TConfig) { }

    /*L'Assassin inflige 10% de degat supplementaire à chaque nouvelle attaque */

    attack() {
        return this.hitStrength *= 1.10
    }
}