import Character from "./Character";
import { TConfig } from "../types";
export default class Hero extends Character {
    name: string;
    health: number;
    hitStrength: number;
    lvl: number;
    xp: number;
    race: string;

    super(config: TConfig) { }
    /*super (){

    }*/

    /*params propre à Hero*/
    //name: le nom donné au Hero par le joueur
    //health: un nombre représentant les points de vie du Hero (ex: 100)
    //hitStrength: un nombre représentant les degat de base qu'inflige une attaque (ex: 30)
    //lvl: un nombre représentant le niveau du Hero,
    // il augmentera grace a l'xp obtenu lors des combats (ex: 1) et augmentera les degats inflifé aux ennemis
    //xp: un nombre représentant l'experience du Hero (ex: 0), sera incrémenté de 2 par combat, quand l'xp atteint 10
    //elle revient à 0 et augmente le lvl de 1
    set_name(){
        this.name;
    }
    set_health(damage: number, etat: string){
        
        if(this.race == "dwarf"){
            let randomIntForDwarf = Math.floor(Math.random() * 101);
            if(randomIntForDwarf <= 20){
                this.health -= damage * 0.5
            }
            else{
                this.health -= damage;
            }
        }
        this.health -= damage;
    }
    set_level(){
        this.lvl;
    }
    set_xp(){
        this.xp += 2;
        if(this.xp == 10){
            this.lvl ++;
            this.xp = 0;
            console.log("GG tu as gagné un niveau !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        }
        this.xp;
    }
    set_race(etat: string): number {
        if(this.race == "humain" && etat == "vol"){
            return this.hitStrength *= 1.1 * this.lvl;
        }
        else if(this.race == "humain" && etat == "sol"){
            return this.hitStrength *= 0.9* this.lvl;
        }
        else if(this.race == "elf" && etat == "vol"){
            return this.hitStrength *= 1.1* this.lvl;
        }
        else if(this.race == "elf" && etat == "sol"){
            return this.hitStrength *= 0.9 * this.lvl;
        }
        else{
            return this.hitStrength;
        }
    }
    attack(etat: string): number {
        return this.set_race(etat);
    }
}