import { TConfig } from "../types";
import Character from "./Character";

export default class Berserker extends Character {

    super(config: TConfig){}

    set_health(damage: number): number {
        return this.health -= damage * 0.7;
    }

}