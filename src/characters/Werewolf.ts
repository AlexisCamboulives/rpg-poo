import Character from "./Character";
import { TConfig } from "../types";
export default class Werewolf extends Character {
    health: number;

    super(config: TConfig) { }

    /*le Wereworf a une resistance de 50% supplementaire*/

    set_health(damage: number): number {
        return this.health -= damage * 0.50
    }
}


/*methode : attack() ou get_hitStrength()?*/
/*diff entre get et set _hitstrenght*/
