
import Battle from './Battle';
import Assassin from './characters/Assasin';
import Berserker from './characters/Berserker';
import Character from './characters/Character';
import Dragon from './characters/Dragon';
import Golem from './characters/Golem';
import Griffin from './characters/Griffin';
import Hero from './characters/Hero';
import Werewolf from './characters/Werewolf';
import { TConfig } from './types';



let configAssassin: TConfig = { name: "Naima", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}
let configBerserker: TConfig = { name: "Mohamed", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}
let configDragon: TConfig = { name: "Philippe", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}
let configGolem: TConfig = { name: "Dylan", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}
let configGriffin: TConfig = { name: "Jimmy", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}
let configHero: TConfig = { name: "Alexis", health: 300, hitStrength: 40, lvl: 1, xp: 0, race: "dwarf", etat: "sol"}
let configWerewolf: TConfig = { name: "Thomas", health: 100, hitStrength: 10, lvl: 1, xp: 0, race: "", etat: "sol"}


let assassin = new Assassin(configAssassin);
let berserker = new Berserker(configBerserker);
let dragon = new Dragon(configDragon);
let golem = new Golem(configGolem);
let griffin = new Griffin(configGriffin);
let hero = new Hero(configHero);
let werewolf = new Werewolf(configWerewolf);
let arr = [];
arr.push(assassin, berserker, dragon, golem, griffin, werewolf);

let battle = new Battle();

for (let i = 0; i < arr.length; i++) {
    battle.fight(hero, arr[i])
    if (hero.get_health() <= 0) {
        console.log("arréter de m'attaqué je suis mourru")
        break;
    }
}
