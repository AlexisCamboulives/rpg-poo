export type TConfig = {
    name: string;
    health: number;
    hitStrength: number;
    lvl: number;
    xp: number;
    race: "humain" | "elf" | "dwarf" | "";
    etat: "fly"|"sol"|"attackFromSky";
}

